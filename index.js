const axios = require('axios')
const express = require('express');
const cors = require('cors');

const app = express();

app.use(cors());

const port = 3000;
const rpcuser = "rpc";
const rpcpass = "123321";
const rpcbind = "127.0.0.1:8332"
const rpccall = `http://${rpcuser}:${rpcpass}@${rpcbind}`;

const rpcSchema = {
  "jsonrpc": "1.0",
  "id": "curltext",
  "method": "",
  "params": [],
};

const rpc = axios.create({baseURL: rpccall,});

app.get('/', (req, res) => {
  res.json({
    version: '1.0'
  });
});

app.get('/getblockcount', async (req, res) => {
  try {
    const result = await rpc.getBlockCount();
    console.log("getblockcount", result);
    res.json(result);
  } catch(e) {
    console.error(e)
    res.status(500).send();
  }
});

app.get('/importaddress/:address', async (req, res) => {
  const { address } = req.params;
  try {
    const result = await rpc.importAddress(address);
    console.log("importaddress", result);
    res.json(result);
  } catch(e) {
    console.error(e)
    res.status(500).send();
  }
});

app.get('/gettransaction/:txid', async (req, res) => {
  const { txid } = req.params;
  try {
    const result = await rpc.getTransaction(txid);
    console.log("gettransaction", result);
    res.json(result);
  } catch(e) {
    console.error(e)
    res.status(500).send();
  }
});

app.get('/listunspent/:address', async (req, res) => {
  const { address } = req.params;
  try {
    const result = await rpc.listUnspent(address);
    console.log("listunspent", result);
    res.json(result);
  } catch(e) {
    console.error(e)
    res.status(500).send();
  }
});

app.get('/getbalance/:address', async (req, res) => {
  const { address } = req.params;
  try {

    const balance = await rpc.getBalance(address);
    console.log("getbalance", balance);
    res.json(balance)
  } catch(e) {
    console.error(e)
    res.status(500).send();
  }
});

app.get('/listtransactions/:address', async (req, res) => {
  const { address } = req.params;
  try {
    const transactions = await rpc.getTransactions(address);
    console.log("listtransactions", transactions);
    res.json(transactions)
  } catch(e) {
    console.error(e)
    res.status(500).send();
  }
});

app.listen(port, "127.0.0.1", () => {
  console.log(`Started at http://localhost:${port}`)
});


rpc.getBlockCount = async () => {
  const response = await rpc.post("/", {...rpcSchema, method: "getblockcount"});
  return response.data.result
};

rpc.importAddress = async (address) => {
  const response = await rpc.post("/", {...rpcSchema, method: "importaddress", params: [address]})
  return response.data.result;
};

rpc.getTransaction = async (txid) => {
  const response = await rpc.post("/", {...rpcSchema, method: "gettransaction", params: [txid, true]})
  const result = response.data.result;
  
  console.log(result);
  return result;
};

rpc.listUnspent = async (address) => {
  const response = await rpc.post("/", {...rpcSchema, method: "listunspent", params: [1, 9999999, [address], true]})
  const result = response.data.result;

  console.log(result);
  return result;
};

rpc.getBalance = async (address) => {
  const response = await rpc.post("/", {...rpcSchema, method: "listaddressgroupings"})
  const addresses = response.data.result;
  const resultAddress = addresses.reduce((acc, item) => {
    return [...acc, ...item];
  }).find((item) => item[0] === address);

  if(!resultAddress) throw new Error(`Address not found: ${address}`)

  return resultAddress[1];
}

rpc.getTransactions = async (address, page = 1, limit = 100) => {
  const data = {
    "method":"listtransactions",
    "params":["*", limit, (page - 1) * limit, true]
  };

  const response = await rpc.post("/", {...rpcSchema, ...data});
  const result = response.data.result;

  console.log(result);
  const transactions = result.filter(item => item.address === address);

  if (!transactions.length) throw new Error(`Transactions not found: ${address}`);
  
  return transactions;
}
